import csv
import matplotlib.pyplot as plt
import random

match_by_year = {}
match_bar = set()
team_name = set()

with open('match.csv') as match_file:
    match_data = csv.DictReader(match_file)

    for match in match_data :
        if match['season'] not in match_by_year.keys():
            match_by_year[match['season']] = {}
            match_by_year[match['season']][match['team1']] = 1
            match_by_year[match['season']][match['team2']] = 1
        else:
            if match['team1'] in match_by_year[match['season']].keys():
                match_by_year[match['season']][match['team1']] += 1
            else:
                match_by_year[match['season']][match['team1']] = 1
            
            if match['team2'] in match_by_year[match['season']].keys():
                match_by_year[match['season']][match['team2']] += 1
            else:
                match_by_year[match['season']][match['team2']] = 1

# print(match_by_year)

r = lambda: random.randint(0,255)

for year,all_match in sorted(match_by_year.items()):
    match_offset = 0
    for team_match in all_match :
        # print(team_match)
        random_color = '#%02X%02X%02X' %(r(), r(), r())
        color = plt.bar(year, all_match[team_match], bottom=match_offset, color= random_color)
        match_bar.add(color)
        team_name.add(team_match)
        match_offset += all_match[team_match]

# print(match_bar)
# print(team_name)


plt.title('Number of Matches Per year')
plt.ylabel('Number of matches')
plt.xlabel('Year')
plt.xticks(rotation = 90)
plt.legend(match_bar, team_name, loc=(0.9,0.6))
plt.show()
