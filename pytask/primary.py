import csv
import matplotlib.pyplot as plt


dict = {}
with open('ps.csv') as csv_file :
       csv_reader = csv.reader(csv_file, delimiter=',')
       line_no = 0
       for row in csv_reader:
              # print(line_no)
              if(line_no == 0):
                     line_no = line_no+1
                     continue

              # line_no = line_no + 1

              cityName = row[8]
              grade = str(row[4])
              if cityName in dict.keys():
                     if grade in dict[cityName].keys():
                            dict[cityName][grade] = 1 + dict[cityName][grade]
                     else:
                            dict[cityName][grade] = 1
              else:
                     dict[cityName] = {}
                     dict[cityName][grade] = 1
              # print(line_no)

# print(dict)

city = []
modelPrimary =[]
lowerPrimary = []
upperPrimary = []
secondary = []
# a = []
# b = []
# c = []
# d = []

for cityName in dict:
       city.append(cityName)
       # a.append(0)
       # b.append(0)
       # c.append(0)
       # d.append(0)
       if 'Upper Primary' in dict[cityName].keys():
              upperPrimary.append(dict[cityName]['Upper Primary'])
       else:
              upperPrimary.append(0)
       
       if 'Lower Primary' in dict[cityName].keys():
              lowerPrimary.append(dict[cityName]['Lower Primary'])
       else:
              lowerPrimary.append(0)

       if 'Secondary' in dict[cityName].keys():
              secondary.append(dict[cityName]['Secondary'])
       else:
              secondary.append(0)
       
       if 'Model Primary' in dict[cityName].keys():
              modelPrimary.append(dict[cityName]['Model Primary'])
       else:
              modelPrimary.append(0)



# print(len(city))
# print(len(modelPrimary))
# print(len(lowerPrimary))
# print(len(secondary))
# print(len(upperPrimary))



width = 0.35

# print("plt.subplot()")
# print(plt.subplot())
fig, ax = plt.subplots()

ax.bar(city, modelPrimary, width, label = 'ModelPrimary')
ax.bar(city, lowerPrimary, width, bottom=modelPrimary, label = 'lowerPrimary')
ax.bar(city, secondary, width,  bottom=lowerPrimary, label='Secondary')
ax.bar(city, upperPrimary, width, bottom = secondary, label = 'upperPrimary')

ax.set_ylabel('Frequency')
ax.set_title('freq in different cities')
ax.legend()

plt.show()




